"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""


#Import required packages

import json
import couchdb
import holoviews as hv
from holoviews import opts
from holoviews.plotting.links import DataLink
import codecs


#Set up the renderer for bokeh

hv.extension('bokeh')
renderer = hv.renderer('bokeh')


#Couchdb credentials

user = "admin"
password = "admin"
couchserver = couchdb.Server("http://%s:%s@172.26.132.16:5984/" % (user, password))
db=couchserver["scenario-sentiment-sa3-iso"]
meldict={}
syddict={}
i=1
for item in db:
    try:
        code=db[item]["SA3_code"]
        if db[item]["City"]=="Melbourne":
            if code in meldict.keys():
                meldict[code]["TotalTweets"]+=1
            else:
                meldict[code]={}
                meldict[code]["TotalTweets"]=1
                meldict[code]["Name"]=db[item]["SA3_name"]
            print(i)
            i=i+1
        else:
            if code in syddict.keys():
                syddict[code]["TotalTweets"]+=1
            else:
                syddict[code]={}
                syddict[code]["TotalTweets"]=1
                syddict[code]["Name"]=db[item]["SA3_name"]
            print(i)
            i=i+1
    except:
        continue
print("db loaded okay")


#Load in the required data for geospatial data, and assign location data to each entry in the previously created dictionaries

with open("SA3_JSON.json") as f:
    jsondata=json.load(f)
delarray=[]
for item in jsondata:
    if (item in meldict) or (item in syddict):
        continue
    else:
        delarray+=[item]
for item in delarray:
    del jsondata[item]


#Create arrays of each dictionary-this is the required format for the chosen visualsation technique (Holoviews choropleth)

melprintarray=[]
for item in meldict:
    try:
        entry=jsondata[item]
        entry["TotalTweets"]=meldict[item]["TotalTweets"]
        entry["ID"]=item
        entry["Name"]=meldict[item]["Name"]
        entry["TweetsPerPop"]=entry["TotalTweets"]/entry["Population"]
        melprintarray.append(entry)
    except:
        continue

sydprintarray=[]
for item in syddict:
    try:
        entry=jsondata[item]
        entry["TotalTweets"]=syddict[item]["TotalTweets"]
        entry["ID"]=item
        entry["Name"]=syddict[item]["Name"]
        entry["TweetsPerPop"]=entry["TotalTweets"]/entry["Population"]
        sydprintarray.append(entry)
    except:
        continue


#Next, we will be creating 4 visualisations. 2 each (one for total tweets, one for tweets per population) for Melbourne and Sydney


#Melbourne tweets per pop first

#Create the map visualisation for tweetsperpop

meltppchoropleth = hv.Polygons(melprintarray, ['lons', 'lats'], ['ID','Name',"Median_Weekly_Income", 'TweetsPerPop'])
meltppchoro=meltppchoropleth.opts(
    opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='TweetsPerPop', colorbar=True, toolbar='above', line_color='white'))


#Also create a table to show numbers

table_data = [(item['Name'], item['Median_Weekly_Income'],item['TweetsPerPop']) for item in melprintarray]
meltable = hv.Table(table_data, [('Name', 'SA3 Area'), 'Median_Weekly_Income','TweetsPerPop'])
meltableoutput=meltable.opts(opts.Table(height=428))


#Create the map visualisation for income

melincchoropleth = hv.Polygons(melprintarray, ['lons', 'lats'], ['ID','Name',"TweetsPerPop", 'Median_Weekly_Income'])
melincchoro=melincchoropleth.opts(
    opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='Median_Weekly_Income', colorbar=True, toolbar='above', line_color='white'))


#Join the table and the choropleth

DataLink(meltableoutput,melincchoropleth)

#Create a joined object of table and the first map

t1=(meltableoutput+melincchoropleth)

#Join this already joined object with the 3rd graph
DataLink(meltableoutput,meltppchoro)
meloutput=(t1+meltppchoro)


#Save the final output as a HTML file
renderer.save(meloutput, 'MelVolTweetsIncomeperpop.html')




#Next, Sydney tweets per pop

#Create the map visualisation for tweetsperpop

sydtppchoropleth = hv.Polygons(sydprintarray, ['lons', 'lats'], ['ID','Name',"Median_Weekly_Income", 'TweetsPerPop'])
sydtppchoro=sydtppchoropleth.opts(
    opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='TweetsPerPop', colorbar=True, toolbar='above', line_color='white'))


#Also create a table to show numbers

table_data = [(item['Name'], item['Median_Weekly_Income'],item['TweetsPerPop']) for item in sydprintarray]
sydtable = hv.Table(table_data, [('Name', 'SA3 Area'), 'Median_Weekly_Income','TweetsPerPop'])
sydtableoutput=sydtable.opts(opts.Table(height=428))


#Create the map visualisation for income

sydincchoropleth = hv.Polygons(sydprintarray, ['lons', 'lats'], ['ID','Name',"TweetsPerPop", 'Median_Weekly_Income'])
sydincchoro=sydincchoropleth.opts(
    opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='Median_Weekly_Income', colorbar=True, toolbar='above', line_color='white'))


#Join the table and the first map

DataLink(sydtableoutput,sydincchoropleth)

#Create a joined object of table and the first map

t1=(sydtableoutput+sydincchoropleth)

#Join this already joined object with the 3rd graph

DataLink(sydtableoutput,sydtppchoro)
sydoutput=(t1+sydtppchoro)


#Save the final output as a HTML file
renderer.save(sydoutput, 'SydVolTweetsIncomeperpop.html')




#Next, Melbourne total tweets

#Create the map visualisation for totaltweets

meltppchoropleth = hv.Polygons(melprintarray, ['lons', 'lats'], ['ID','Name',"Median_Weekly_Income", 'TotalTweets'])
meltppchoro=meltppchoropleth.opts(
    opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='TotalTweets', colorbar=True, toolbar='above', line_color='white'))


#Also create a table to show numbers

table_data = [(item['Name'], item['Median_Weekly_Income'],item['TotalTweets']) for item in melprintarray]
meltable = hv.Table(table_data, [('Name', 'SA3 Area'), 'Median_Weekly_Income','TotalTweets'])
meltableoutput=meltable.opts(opts.Table(height=428))


#Create the map visualisation for income

melincchoropleth = hv.Polygons(melprintarray, ['lons', 'lats'], ['ID','Name',"TotalTweets", 'Median_Weekly_Income'])
melincchoro=melincchoropleth.opts(
    opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='Median_Weekly_Income', colorbar=True, toolbar='above', line_color='white'))


#Join the table and the first map

DataLink(meltableoutput,melincchoropleth)
t1=(meltableoutput+melincchoropleth)

#Join this already joined object with the 3rd graph

DataLink(meltableoutput,meltppchoro)
meloutput=(t1+meltppchoro)


#Save the final output as a HTML file
renderer.save(meloutput, 'MelVolTweetsIncometotal.html')




#Finally, sydney total tweets

#Create the map visualisation for totaltweets

sydtppchoropleth = hv.Polygons(sydprintarray, ['lons', 'lats'], ['ID','Name',"Median_Weekly_Income", 'TotalTweets'])
sydtppchoro=sydtppchoropleth.opts(
    opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='TotalTweets', colorbar=True, toolbar='above', line_color='white'))


#Also create a table to show numbers

table_data = [(item['Name'], item['Median_Weekly_Income'],item['TotalTweets']) for item in sydprintarray]
sydtable = hv.Table(table_data, [('Name', 'SA3 Area'), 'Median_Weekly_Income','TotalTweets'])
sydtableoutput=sydtable.opts(opts.Table(height=428))


#Create the map visualisation for income

sydincchoropleth = hv.Polygons(sydprintarray, ['lons', 'lats'], ['ID','Name',"TotalTweets", 'Median_Weekly_Income'])
sydincchoro=sydincchoropleth.opts(
    opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='Median_Weekly_Income', colorbar=True, toolbar='above', line_color='white'))


#Join the table and the first map

DataLink(sydtableoutput,sydincchoropleth)
t1=(sydtableoutput+sydincchoropleth)

#Join this already joined object with the 3rd graph

DataLink(sydtableoutput,sydtppchoro)
sydoutput=(t1+sydtppchoro)


#Save the final output as a HTML file

renderer.save(sydoutput, 'SydVolTweetsIncometotal.html')



#If the html db does not exist, it is created and placeholder values are set. If it does, then it is accessed.

if "htmldb" in couchserver:
    db=couchserver["htmldb"]
    print("htmldb database already exists")
else:
    db=couchserver.create("htmldb")
    db["AQSentiment"]={"Category":"AQSentiment","htmlcode":"blank"}
    db["MelHospitals"]={"Category":"ClosestHospital","htmlcode":"blank"}
    db["MelIsoSentiment"]={"Category":"IsoSentiment","htmlcode":"blank"}
    db["MelVolTweetsIncomeperpop"]={"Category":"IsoSentiment","htmlcode":"blank"}
    db["MelVolTweetsIncometotal"]={"Category":"MelVolTweetsIncometotal","htmlcode":"blank"}
    db["SydHospitals"]={"Category":"SydHospitals","htmlcode":"blank"}
    db["SydIsoSentiment"]={"Category":"SydIsoSentiment","htmlcode":"blank"}
    db["SydVolTweetsIncomeperpop"]={"Category":"SydVolTweetsIncomeperpop","htmlcode":"blank"}
    db["SydVolTweetsIncometotal"]={"Category":"SydVolTweetsIncomeperpop","htmlcode":"blank"}
    print("new database created: htmldb")



#All of the earlier created Melbourne and Sydney HTML files are read in again, and are uploaded to the couchdb

with codecs.open("MelVolTweetsIncomeperpop.html","r") as f:
    item=f.read()
doc=db.get("MelVolTweetsIncomeperpop")
doc["htmlcode"]=item
db.save(doc)
with codecs.open("MelVolTweetsIncometotal.html","r") as f:
    item=f.read()
doc=db.get("MelVolTweetsIncometotal")
doc["htmlcode"]=item
db.save(doc)
with codecs.open("SydVolTweetsIncomeperpop.html","r") as f:
    item=f.read()
doc=db.get("SydVolTweetsIncomeperpop")
doc["htmlcode"]=item
db.save(doc)
with codecs.open("SydVolTweetsIncometotal.html","r") as f:
    item=f.read()
doc=db.get("SydVolTweetsIncometotal")
doc["htmlcode"]=item
db.save(doc)
