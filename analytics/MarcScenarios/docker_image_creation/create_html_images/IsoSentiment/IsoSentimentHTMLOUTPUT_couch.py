"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""


#Import required packages

import json
import couchdb
import holoviews as hv
from holoviews import opts
from holoviews.plotting.links import DataLink
import codecs


#Set up the renderer for bokeh

hv.extension('bokeh')
renderer = hv.renderer('bokeh')


#Couchdb credentials

user = "admin"
password = "admin"
couchserver = couchdb.Server("http://%s:%s@172.26.132.16:5984/" % (user, password))
db=couchserver["scenario-sentiment-sa3-iso"]


#load in the data from the database, and summarise by finding total sentiment and total number of tweets for each area. Separate by city to create two visualisation later.

meldict={}
syddict={}
i=1
for item in db:
    try:


        #Only load in the tweets mentioning iso

        if db[item]["isomention"]==0:
            print(i)
            i=i+1
            continue
        code=db[item]["SA3_code"]
        if db[item]["City"]=="Melbourne":
            if code in meldict.keys():
                meldict[code]["TotalTweets"]+=1
                meldict[code]["TotalSentiment"]+=db[item]["Sentiment"]
            else:
                meldict[code]={}
                meldict[code]["TotalTweets"]=1
                meldict[code]["TotalSentiment"]=db[item]["Sentiment"]
                meldict[code]["Name"]=db[item]["SA3_name"]
            print(i)
            i=i+1
        else:
            if code in meldict.keys():
                syddict[code]["TotalTweets"]+=1
                syddict[code]["TotalSentiment"]+=db[item]["Sentiment"]
            else:
                syddict[code]={}
                syddict[code]["TotalTweets"]=1
                syddict[code]["TotalSentiment"]=db[item]["Sentiment"]
                syddict[code]["Name"]=db[item]["SA3_name"]
            print(i)
            i=i+1
    except:
        continue


#Load in the required data for geospatial data, and assign location data to each entry in the previously created dictionaries

with open("SA3_JSON.json") as f:
    jsondata=json.load(f)
delarray=[]
for item in jsondata:
    if (item in meldict) or (item in syddict):
        continue
    else:
        delarray+=[item]
for item in delarray:
    del jsondata[item]


#Create arrays of each dictionary-this is the required format for the chosen visualsation technique (Holoviews choropleth)

melprintarray=[]
for item in meldict:
    try:
        entry=jsondata[item]
        entry["TotalTweets"]=meldict[item]["TotalTweets"]
        entry["TotalSentiment"]=meldict[item]["TotalSentiment"]
        entry["AvgSentiment"]=entry["TotalSentiment"]/entry["TotalTweets"]
        entry["ID"]=item
        entry["Name"]=meldict[item]["Name"]
        melprintarray.append(entry)
    except:
        continue
sydprintarray=[]
for item in syddict:
    try:
        entry=jsondata[item]
        entry["TotalTweets"]=syddict[item]["TotalTweets"]
        entry["TotalSentiment"]=syddict[item]["TotalSentiment"]
        entry["AvgSentiment"]=entry["TotalSentiment"]/entry["TotalTweets"]
        entry["ID"]=item
        entry["Name"]=syddict[item]["Name"]
        sydprintarray.append(entry)
    except:
        continue


#Create the Melbourne map visualisation

melchoropleth = hv.Polygons(melprintarray, ['lons', 'lats'], ['ID','Name', 'AvgSentiment'],label="Melbourne's sentiment regarding isolation")


#Create a table to sit alongside the map

table_data = [(item['Name'], item['AvgSentiment']) for item in melprintarray]
meltable = hv.Table(table_data, [('Name', 'SA3 Area'), 'AvgSentiment'])


#Link the map and the table so clicking one affects the other

dlink=DataLink(melchoropleth, meltable)


#Create a holoviews object and store it as a HTML file

melchoro=(melchoropleth+meltable).opts(
        opts.Table(height=428),
        opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='AvgSentiment', colorbar=True, toolbar='above', line_color='white'))
renderer.save(melchoro, 'MelIsoSentiment.html')
dlink.unlink()


#Create the Sydney map visualisation

sydchoropleth = hv.Polygons(sydprintarray, ['lons', 'lats'], ['ID','Name', 'AvgSentiment'],label="Sydney's sentiment regarding isolation")


#Create a table to sit alongside the map

table_data = [(item['Name'], item['AvgSentiment']) for item in sydprintarray]
sydtable = hv.Table(table_data, [('Name', 'SA3 Area'), 'AvgSentiment'])


#Link the map and the table so clicking one affects the other

dlink=DataLink(sydchoropleth, sydtable)


#Create a holoviews object and store it as a HTML file

sydchoro=(sydchoropleth+sydtable).opts(
        opts.Table(height=428),
        opts.Polygons(logz=True, tools=['hover'], xaxis=None, yaxis=None,
                   show_grid=False, show_frame=False, width=500, height=500,
                   color_index='AvgSentiment', colorbar=True, toolbar='above', line_color='white'))
renderer.save(sydchoro, 'SydIsoSentiment.html')
dlink.unlink()


#If the html db does not exist, it is created and placeholder values are set. If it does, then it is accessed.

if "htmldb" in couchserver:
    db=couchserver["htmldb"]
    print("htmldb database already exists")
else:
    db=couchserver.create("htmldb")
    db["AQSentiment"]={"Category":"AQSentiment","htmlcode":"blank"}
    db["MelHospitals"]={"Category":"ClosestHospital","htmlcode":"blank"}
    db["MelIsoSentiment"]={"Category":"IsoSentiment","htmlcode":"blank"}
    db["MelVolTweetsIncomeperpop"]={"Category":"IsoSentiment","htmlcode":"blank"}
    db["MelVolTweetsIncometotal"]={"Category":"MelVolTweetsIncometotal","htmlcode":"blank"}
    db["SydHospitals"]={"Category":"SydHospitals","htmlcode":"blank"}
    db["SydIsoSentiment"]={"Category":"SydIsoSentiment","htmlcode":"blank"}
    db["SydVolTweetsIncomeperpop"]={"Category":"SydVolTweetsIncomeperpop","htmlcode":"blank"}
    db["SydVolTweetsIncometotal"]={"Category":"SydVolTweetsIncomeperpop","htmlcode":"blank"}
    print("new database created: htmldb")


#Both the earlier created Melbourne and Sydney HTML files are read in again, and are uploaded to the couchdb

with codecs.open("MelIsoSentiment.html","r") as f:
    item=f.read()
doc=db.get("MelIsoSentiment")
doc["htmlcode"]=item
db.save(doc)

with codecs.open("SydIsoSentiment.html","r") as f:
    item=f.read()
doc=db.get("SydIsoSentiment")
doc["htmlcode"]=item
db.save(doc)
