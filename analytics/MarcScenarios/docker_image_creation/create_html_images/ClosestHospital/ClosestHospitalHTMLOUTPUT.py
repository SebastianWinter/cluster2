"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""


#Import required packages

import json
import couchdb
from bokeh.resources import CDN
from bokeh.embed import file_html
from bokeh.models import GeoJSONDataSource, GMapOptions,HoverTool
from bokeh.plotting import gmap
import re
import holoviews as hv

#Set up the renderer for bokeh

hv.extension('bokeh')
renderer = hv.renderer('bokeh')

#Couchdb credentials

user = "admin"
password = "admin"
couchserver = couchdb.Server("http://%s:%s@172.26.132.16:5984/" % (user, password))
db=couchserver["scenario-closesthospital"]

#Load in the required datasets and initialise any values needed for analyses

with open("HOSPITALS_MEL.json") as f:
    meldata = json.load(f)
melnewfeats=[]
for item in meldata["features"]:
    hospname=item["properties"]["hospital_name"]
    if not re.search("hospital",hospname.lower()):
        continue
    else:
        item["properties"]["TweetCount"]=0
        melnewfeats+=[item]
meldata["features"]=melnewfeats
print("mel hospitals cut")
with open("HOSPITALS_SYD.json") as f:
    syddata = json.load(f)
sydnewfeats=[]
for item in syddata["features"]:
    hospname=item["properties"]["hospital_name"]
    if not re.search("hospital",hospname.lower()):
        continue
    else:
        item["properties"]["TweetCount"]=0
        sydnewfeats+=[item]

syddata["features"]=sydnewfeats
print("syd hospitals cut")

#Items in the scenario database are loaded in, and the relevant dictionary counts the number of entries associated with it

i=1
for item in db:
    try:
        name=db[item]["Hospital"]
        if db[item]["City"]=="Melbourne":
            for hosp in meldata["features"]:
                if name==hosp["properties"]["hospital_name"]:
                    hosp["properties"]["TweetCount"]+=1
        else:
            for hosp in syddata["features"]:
                if name==hosp["properties"]["hospital_name"]:
                    hosp["properties"]["TweetCount"]+=1
        print(i)
        i=i+1
    except:
        continue


#the visualisation is created, first for the Melbourne hospitals

map_options = GMapOptions(lat=-37.8057138, lng=144.9241773, map_type="roadmap", zoom=11)
mel_source = GeoJSONDataSource(geojson=json.dumps(meldata))
TOOLTIPS = [
    ('Hospital', '@hospital_name'),('Tweet Count','@TweetCount')
]


#Note that a google maps api key is required

melmap = gmap("AIzaSyBx8W4xCuernfI0lfYGh5NMyKzOJpE0N0I", map_options, title="Hospitals in Melbourne")


#Hospitals are shown as circles on the map

melmap.circle(x='x', y='y', size=10, color='blue', alpha=0.7, source=mel_source)
melmap.add_tools( HoverTool(tooltips=TOOLTIPS))


#The map is output as a HTML file

html = file_html(melmap, CDN, "MelHospitals.html")
with open("/htmloutput/MelHospitals.html","w") as f:
    f.write(html)


#the same is repeated for the Sydney hospitals

map_options = GMapOptions(lat=-33.8185089, lng=150.8660976, map_type="roadmap", zoom=11)

syd_source = GeoJSONDataSource(geojson=json.dumps(syddata))
TOOLTIPS = [
    ('Hospital', '@hospital_name'),('Tweet Count','@TweetCount')
]


#Note that a google maps api key is required

sydmap = gmap("AIzaSyBx8W4xCuernfI0lfYGh5NMyKzOJpE0N0I", map_options, title="Hospitals in Sydney")
sydmap.circle(x='x', y='y', size=10, color='red', alpha=0.7, source=syd_source)
sydmap.add_tools( HoverTool(tooltips=TOOLTIPS))


#The map is output as a HTML file

html = file_html(sydmap, CDN, "SydHospitals.html")
with open("/htmloutput/SydHospitals.html","w") as f:
    f.write(html)
