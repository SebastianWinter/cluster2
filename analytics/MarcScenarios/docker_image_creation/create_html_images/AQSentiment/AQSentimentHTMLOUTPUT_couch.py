"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""


#Import required packages

import couchdb
import holoviews as hv
from holoviews import opts
import pandas as pd
import codecs

#Set up the renderer for bokeh

hv.extension('bokeh')

#Couchdb credentials

user = "admin"
password = "admin"
couchserver = couchdb.Server("http://%s:%s@172.26.132.16:5984/" % (user, password))
db=couchserver["scenario-aqsentiment"]

#Create a dataframe which will be used for visualisation purposes

BIGDFcols={"PM 10 Reading":[],"SO2 Reading":[],"NO2 Reading":[],"PM 2.5 Reading":[],"CO Reading":[],"O3 Reading":[],"Sentiment":[]}
BIGDF=pd.DataFrame(BIGDFcols,columns=["PM 10 Reading","SO2 Reading","NO2 Reading","PM 2.5 Reading","CO Reading","O3 Reading","Sentiment"])
columns={"Reading":[],"Sentiment":[]}
PM10_df=pd.DataFrame(columns,columns=["Reading","Sentiment"])
SO2_df=pd.DataFrame(columns,columns=["Reading","Sentiment"])
NO2_df=pd.DataFrame(columns,columns=["Reading","Sentiment"])
PM25_df=pd.DataFrame(columns,columns=["Reading","Sentiment"])
CO_df=pd.DataFrame(columns,columns=["Reading","Sentiment"])
O3_df=pd.DataFrame(columns,columns=["Reading","Sentiment"])
print("df making complete")

#input each item in the scenario db into the dataframe

i=0
for item in db:
    try:
        sentiment=db[item]["Sentiment"]
        PM10_df=PM10_df.append({"Reading":db[item]["PM10_level"],"Sentiment":sentiment},ignore_index=True)
        SO2_df=SO2_df.append({"Reading":db[item]["SO2_level"],"Sentiment":sentiment},ignore_index=True)
        NO2_df=NO2_df.append({"Reading":db[item]["NO2_level"],"Sentiment":sentiment},ignore_index=True)
        PM25_df=PM25_df.append({"Reading":db[item]["PM2.5_level"],"Sentiment":sentiment},ignore_index=True)
        CO_df=CO_df.append({"Reading":db[item]["CO_level"],"Sentiment":sentiment},ignore_index=True)
        O3_df=O3_df.append({"Reading":db[item]["O3_level"],"Sentiment":sentiment},ignore_index=True)
        BIGDF=BIGDF.append({"PM 10 Reading":db[item]["PM10_level"],"SO2 Reading":db[item]["SO2_level"],"NO2 Reading":db[item]["NO2_level"],"PM 2.5 Reading":db[item]["PM2.5_level"],"CO Reading":db[item]["CO_level"],"O3 Reading":db[item]["O3_level"],"Sentiment":sentiment,},ignore_index=True)
        i=i+1
        print(i)
    except:
        continue


#Create the visualisations

title = "PM10 sentiment distribution"
boxwhisker = hv.BoxWhisker(BIGDF, "PM 10 Reading", 'Sentiment', label=title).sort()
PM10whisk=boxwhisker.opts(show_legend=False, width=600)

title = "SO2 sentiment distribution"
boxwhisker = hv.BoxWhisker(BIGDF, "SO2 Reading", 'Sentiment', label=title).sort()
SO2whisk=boxwhisker.opts(show_legend=False, width=600)

title = "NO2 sentiment distribution"
boxwhisker = hv.BoxWhisker(BIGDF, "NO2 Reading", 'Sentiment', label=title).sort()
NO2whisk=boxwhisker.opts(show_legend=False, width=600)

title = "PM2.5 sentiment distribution"
boxwhisker = hv.BoxWhisker(BIGDF, "PM 2.5 Reading", 'Sentiment', label=title).sort()
PM25whisk=boxwhisker.opts(show_legend=False, width=600)

title = "CO sentiment distribution"
boxwhisker = hv.BoxWhisker(BIGDF, "CO Reading", 'Sentiment', label=title).sort()
COwhisk=boxwhisker.opts(show_legend=False, width=600)

title = "O3 sentiment distribution"
boxwhisker = hv.BoxWhisker(BIGDF, "O3 Reading", 'Sentiment', label=title).sort()
O3whisk=boxwhisker.opts(show_legend=False, width=600)

fullplot=hv.Layout(PM10whisk+SO2whisk+NO2whisk+PM25whisk+COwhisk+O3whisk).cols(3)
renderer = hv.renderer('bokeh')
renderer.save(fullplot, 'AQSentiment.html')

#If the html db does not exist, it is created and placeholder values are set. If it does, then it is accessed.

if "htmldb" in couchserver:
    db=couchserver["htmldb"]
    print("htmldb database already exists")
else:
    db=couchserver.create("htmldb")
    db["AQSentiment"]={"Category":"AQSentiment","htmlcode":"blank"}
    db["MelHospitals"]={"Category":"ClosestHospital","htmlcode":"blank"}
    db["MelIsoSentiment"]={"Category":"IsoSentiment","htmlcode":"blank"}
    db["MelVolTweetsIncomeperpop"]={"Category":"IsoSentiment","htmlcode":"blank"}
    db["MelVolTweetsIncometotal"]={"Category":"MelVolTweetsIncometotal","htmlcode":"blank"}
    db["SydHospitals"]={"Category":"SydHospitals","htmlcode":"blank"}
    db["SydIsoSentiment"]={"Category":"SydIsoSentiment","htmlcode":"blank"}
    db["SydVolTweetsIncomeperpop"]={"Category":"SydVolTweetsIncomeperpop","htmlcode":"blank"}
    db["SydVolTweetsIncometotal"]={"Category":"SydVolTweetsIncomeperpop","htmlcode":"blank"}
    print("new database created: htmldb")

#The required entry is requested from the couchdb
with codecs.open("AQSentiment.html","r") as f:
    item=f.read()


#And updated, before it saved to the couchdb again

doc=db.get("AQSentiment")
doc["htmlcode"]=item
db.save(doc)
