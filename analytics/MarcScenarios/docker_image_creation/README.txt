"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""


The create_upload_images directory contains the relevant code required to create Docker images to parse through the harvester database, process tweets and upload them to scenario databases. Required files for the image and the Dockerfile used to create the image are included.

The create_html_images directory contains the relevant code required to create Docker images to parse through the scenario databases, aggregate data and output the summary visually as html. Required files for the image and the Dockerfile used to create the image are included.

Note that in some of the subdirectories of the create_html_images directory, there are two dockerfiles and two python scripts. The Dockerfile_couch files were used in conjunction with the python scripts with a _couch suffix in order to upload the html outputs to a couchdb container rather than as a html file. For use, the existing Dockerfile would need to be removed (or renamed) and the Dockerfile_couch would need to be renamed to Dockerfile.

It is prudent to mention that none of the scripts which analyse the scenario databases utilise views. This is because when the HTML creating was ready to begin, views on our couchdb stopped working. Code could easily be adjusted to use views rather than the for loops included in the current iteration of code. In addition to code being simpler, it would also run faster.
