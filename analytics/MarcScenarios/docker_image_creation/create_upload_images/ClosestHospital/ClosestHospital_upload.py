"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""

#Import required packages

import datetime
import json
import re
import math
import couchdb

#Include a timer for runtime comparison

start=datetime.datetime.now()

#Couchdb credentials

user = "admin"
password = "admin"
couchserver = couchdb.Server("http://%s:%s@172.26.132.16:5984/" % (user, password))
db=couchserver["mydb"]

#If the scenario db does not exist, it is created. If it does, then it is accessed.

if "scenario-closesthospital" in couchserver:
    db2=couchserver["scenario-closesthospital"]
    print("scenario-closesthospital database already exists")
else:
    db2=couchserver.create("scenario-closesthospital")
    print("new database created: scenario-closesthospital")


#Data required for analysed is loaded. Two separate dictionaries are made to avoid a single tweet testing entries in the wrong city.

with open("HOSPITALS_SYD.json","r", encoding='utf-8') as f:
    syd_data=json.load(f)
with open("HOSPITALS_MEL.json","r", encoding='utf-8') as f:
    mel_data=json.load(f)    
mel_hospdict={}
mel_delarray=[]
for item in mel_data["features"]:
    hospname=item["properties"]["hospital_name"]
    coords=item["geometry"]["coordinates"][::-1]
    mel_hospdict[hospname]={}
    mel_hospdict[hospname]["hospital_name"]=hospname
    mel_hospdict[hospname]["coords"]=coords

    # Entries without "hospital" in their name are filtered out, as they are not related to the scenario

    if not re.search("hospital",hospname.lower()):
        mel_delarray+=[hospname]

for item in mel_delarray:
    del mel_hospdict[item]

syd_hospdict={}
syd_delarray=[]
for item in mel_data["features"]:
    hospname=item["properties"]["hospital_name"]
    coords=item["geometry"]["coordinates"][::-1]
    syd_hospdict[hospname]={}
    syd_hospdict[hospname]["hospital_name"]=hospname
    syd_hospdict[hospname]["coords"]=coords

    # Entries without "hospital" in their name are filtered out, as they are not related to the scenario

    if not re.search("hospital",hospname.lower()):
        syd_delarray+=[hospname]

for item in syd_delarray:
    del syd_hospdict[item]

hosploadtime=datetime.datetime.now()
timediff=hosploadtime-start
print("time taken to load hospital json: {} seconds".format(timediff.seconds))


#Each tweet in the database view (that is, those which have geolocation enabled) is compared to all of the hospitals in the tweet's city (using a bounding box), and the closest hospital entry in the above dictionary has it's counter updated with how many tweets it is assigned.
#A counter is initialised to examine how many "new" tweets are processed

i=0
for item in db.view("test/text_coords"):
    _id=item.key[0]
    if item.key[0] in db2:
        continue
    else:
        #below segment filters out tweets without "corona" or "covid" mentions, as required of the scenario

        if not (re.search("covid",item.key[1].lower()) or re.search("corona",item.key[1].lower())):
            continue
        
        #Bounding box to reduce how many hospitals each tweet has to parse through

        coords=item.key[2]["coordinates"][::-1]
        if coords[0]>-34.325 and coords[0]<-33.3019 and coords[1]>150.2018 and coords[1]<151.5756:
            hospdict=syd_hospdict
            city="Sydney"
        elif coords[0]>-38.5011 and coords[0]<-37.2495 and coords[1]>144.3244 and coords[1]<145.8213:
            hospdict=mel_hospdict
            city="Melbourne"
        else:
            continue
        mydist=64
        closesthosp=None
        for hospital in hospdict:
            hcoords=hospdict[hospital]["coords"]
            tempdistance=math.sqrt((coords[0]-hcoords[0])**2+(coords[1]-hcoords[1])**2)
            if tempdistance<mydist:
                mydist=tempdistance
                closesthosp=hospital
            else:
                continue

#Processed entries are uplaoded to the scenario database

        db2[item.key[0]]={"_id":_id,"Hospital":closesthosp,"City":city}
        i=i+1


#metrics are output which can be examined in docker logs

Analysistime=datetime.datetime.now()
timediff=Analysistime-hosploadtime
print("time taken to assign tweets to hospitals and analyse data: {} seconds".format(timediff.seconds))
print("number of tweets uploaded to couchdb: {}".format(i))
