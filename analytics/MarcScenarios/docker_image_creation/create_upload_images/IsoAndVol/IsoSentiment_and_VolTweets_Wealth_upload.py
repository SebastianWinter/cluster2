"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""

#Import required packages

import datetime
import json
import shapefile
import re
import couchdb
import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from textblob import TextBlob

#Include a timer for runtime comparison

start=datetime.datetime.now()

#Couchdb credentials

user = "admin"
password = "admin"
couchserver = couchdb.Server("http://%s:%s@172.26.132.16:5984/" % (user, password))
db=couchserver["mydb"]


#If the scenario db does not exist, it is created. If it does, then it is accessed.

if "scenario-sentiment-sa3-iso" in couchserver:
    db2=couchserver["scenario-sentiment-sa3-iso"]
    print("scenario-sentiment-sa3-iso database already exists")
else:
    db2=couchserver.create("scenario-sentiment-sa3-iso")
    print("new database created: scenario-sentiment-sa3-iso")


#Required data in the form of a shapefile is loaded, and a dictionary is created to provide properties about each area, but also to store polygons made for tweets coordinates to be compared to. This will allow tweets to be assigned to SA3 area.

sf=shapefile.Reader("SA3_2016_AUST.shp")
shapes=sf.shapes() 

SA3dict={}
errorarray=[]
errornamearray=[]
i=0
incomeerror=0
incomeerrorarray=[]
while i<len(sf):
    try:
        identifier=sf.record(i)['SA3_CODE16']
        SA3dict[identifier]={}
        SA3dict[identifier]["5dig"]=sf.record(i)['SA3_CODE16']
        SA3dict[identifier]["Name"]=sf.record(i)['SA3_NAME16']
        coords=[]
        for item in shapes[i].points:
            entry=[]
            for coord in item:
                entry+=[coord]
            coords+=[entry]
        testlats=np.array([])
        testlongs=np.array([])
        for coord in coords:
            testlats=np.append(testlats,coord[1])
            testlongs=np.append(testlongs,coord[0])
        lons_lats_vect = np.column_stack((testlats, testlongs))
        polygon=Polygon(lons_lats_vect)
        SA3dict[identifier]["Polygon"]=polygon
        i+=1
    except:
        errorarray+=[i]
        errornamearray+=[identifier]
        print("error-Area:{}".format(identifier))
        i=i+1

Subloadtime=datetime.datetime.now()
timediff=Subloadtime-start
print("time taken to load Areas and assign polygons: {} seconds".format(timediff.seconds))


#Each tweet in the database view (that is, those which have geolocation enabled) is assigned the SA3 area it is located in
#A counter is initialised to examine how many "new" tweets are processed

i=0
for item in db.view("test/text_coords"):
    if item.key[0] in db2:
        continue
    else:
        _id=item.key[0]

        #below segment filters out tweets without "iso" mentions, as required of one of the scenarios using the processed data

        if not (re.search("iso",item.key[1].lower())):
            isopresent=0
        else:
            isopresent=1

        
        #Tweets are assigned cities to streamline future analyses

        coords=item.key[2]["coordinates"][::-1]
        if coords[0]>-34.325 and coords[0]<-33.3019 and coords[1]>150.2018 and coords[1]<151.5756:
            city="Sydney"
        elif coords[0]>-38.5011 and coords[0]<-37.2495 and coords[1]>144.3244 and coords[1]<145.8213:
            city="Melbourne"
        else:
            continue
        mypoint=Point(coords)
        for area in SA3dict:
            try:
                if SA3dict[area]["Polygon"].contains(mypoint):
                    _sentiment=TextBlob(item.key[1].lower()).sentiment.polarity


#Processed entries are uplaoded to the scenario database

                    db2[_id]={"_id":_id,"SA3_name":SA3dict[area]["Name"],"SA3_code":area,"Sentiment":_sentiment,"City":city,"isomention":isopresent}
                    i=i+1
                    break
            except:
                continue


#metrics are output which can be examined in docker logs

Analysistime=datetime.datetime.now()
timediff=Analysistime-start
print("time taken to assign tweets to locations and analyse: {} seconds".format(timediff.seconds))
print("number of tweets uploaded to cloud: {}".format(i))
