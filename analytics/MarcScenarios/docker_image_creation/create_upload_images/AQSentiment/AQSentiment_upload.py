"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""

#Import required packages

import datetime
import json
import re
import math
import couchdb
from textblob import TextBlob

#Include a timer for runtime comparison

start=datetime.datetime.now()


#Couchdb credentials

user = "admin"
password = "admin"
couchserver = couchdb.Server("http://%s:%s@172.26.132.16:5984/" % (user, password))
db=couchserver["mydb"]

#If the scenario db does not exist, it is created. If it does, then it is accessed.

if "scenario-aqsentiment" in couchserver:
    db2=couchserver["scenario-aqsentiment"]
    print("scenario-aqsentiment database already exists")
else:
    db2=couchserver.create("scenario-aqsentiment")
    print("new database created: scenario-aqsentiment")


#Required data to aid processing added

with open("AirQuality.json","r", encoding='utf-8') as f:
    data=json.load(f)


#The AirQuality data is aggregated to find averages for each type of particle

AQdict={}
donearray=[]

for item in data["features"]:
    if item in donearray:
        AQdict[item]["PM10"]=(AQdict[item]["PM10"]*AQdict[item]["record_count"]+item["properties"]["PM10"])/(AQdict[item]["record_count"]+1)
        AQdict[item]["SO2"]=(AQdict[item]["SO2"]*AQdict[item]["record_count"]+item["properties"]["SO2"])/(AQdict[item]["record_count"]+1)
        AQdict[item]["NO2"]=(AQdict[item]["NO2"]*AQdict[item]["record_count"]+item["properties"]["NO2"])/(AQdict[item]["record_count"]+1)
        AQdict[item]["PM2.5"]=(AQdict[item]["PM2.5"]*AQdict[item]["record_count"]+item["properties"]["PM2.5"])/(AQdict[item]["record_count"]+1)
        AQdict[item]["CO"]=(AQdict[item]["CO"]*AQdict[item]["record_count"]+item["properties"]["CO"])/(AQdict[item]["record_count"]+1)
        AQdict[item]["O3"]=(AQdict[item]["O3"]*AQdict[item]["record_count"]+item["properties"]["O3"])/(AQdict[item]["record_count"]+1)
    else:
        sitename=item["properties"]["Station"]
        coords=item["geometry"]["coordinates"][::-1]
        AQdict[sitename]={}
        AQdict[sitename]["Station"]=sitename
        AQdict[sitename]["coords"]=coords
        AQdict[sitename]["tweetcount"]=0
        AQdict[sitename]["PM10"]=item["properties"]["PM10"]
        AQdict[sitename]["SO2"]=item["properties"]["SO2"]
        AQdict[sitename]["NO2"]=item["properties"]["NO2"]
        AQdict[sitename]["PM2.5"]=item["properties"]["PM2.5"]
        AQdict[sitename]["CO"]=item["properties"]["CO"]
        AQdict[sitename]["O3"]=item["properties"]["O3"]
        donearray+=[sitename]

AQloadtime=datetime.datetime.now()
timediff=AQloadtime-start
print("time taken to load AQ json: {} seconds".format(timediff.seconds))


#Each tweet in the database view (that is, those which have geolocation enabled) is assigned the readings of it's closest air quality sensor
#A counter is initialised to examine how many "new" tweets are processed

i=0
for item in db.view("test/text_coords"):
    if item.key[0] in db2:
        continue
    else:
        coords=item.key[2]["coordinates"][::-1]
        if coords[0]<-34.325 or coords[0]>-33.3019 or coords[1]<150.2018 or coords[1]>151.5756:
            continue
        else:
            _id=item.key[0]
            coords=item.key[2]["coordinates"]
            mydist=64
            closestsite=None
            for site in AQdict:
                AQcoords=AQdict[site]["coords"]
                tempdistance=math.sqrt((coords[0]-AQcoords[0])**2+(coords[1]-AQcoords[1])**2)
                if tempdistance<mydist:
                    mydist=tempdistance
                    closestsite=site
                else:
                    continue
            sentimentval=TextBlob(text).sentiment.polarity

#Processed entries are uplaoded to the scenario database

            db2[item.key[0]]={"_id":_id,"Closest_site":closestsite,"Sentiment":sentimentval,"PM10_level":AQdict[sitename]["PM10"],"SO2_level":AQdict[sitename]["SO2"],"NO2_level":AQdict[sitename]["NO2"],"PM2.5_level":AQdict[sitename]["PM2.5"],"CO_level":AQdict[sitename]["CO"],"O3_level":AQdict[sitename]["O3"]}
            i=i+1
print("code complete")

#metrics are output which can be examined in docker logs

Analysistime=datetime.datetime.now()
timediff=Analysistime-AQloadtime
print("time taken to assign tweets to locations and analyse sentiment: {} seconds".format(timediff.seconds))
print("number of tweets uploaded to couchdb: {}".format(i))
