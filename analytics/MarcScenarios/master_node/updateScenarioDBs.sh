docker service create -e HTTPS_PROXY=http://wwwproxy.unimelb.edu.au:8000/ --name isoandvolupdate marcnguyen/ccass2:isoandvolload
docker service create -e HTTPS_PROXY=http://wwwproxy.unimelb.edu.au:8000/ --name closesthospupdate marcnguyen/ccass2:closesthospitalload
docker service create -e HTTPS_PROXY=http://wwwproxy.unimelb.edu.au:8000/ --name aqsentimentupdate marcnguyen/ccass2:aqsentimentload
