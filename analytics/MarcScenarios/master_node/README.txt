"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""

As the shell scripts in this directory assume that the node is the master of a Docker swarm, they should only be run on such a node.

The pullLoadImages and pullHTMLimages shell scripts to pull the required images to run the upload and analyses containers on a swarm.

The updateScenarioDBs and createHTMLs shell scripts create services related to their respective tasks on the Docker swarm.

Note that pullHTMLimages.sh createHTMLs.sh relate to the HTML create images which are in the docker_image_creation folder of this repository. They relate to creating HTMLs which are then uploaded to a new HTML couchdb database. This option was not used for our final frontend, though is included here for future potential.
