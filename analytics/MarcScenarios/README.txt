"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""


Inside the data_used directory are the files sourced from AURIN and the ABS used in analyses.

In the docker_image_creation directory are subdirectories containing the files and Dockerfiles required to create each image used for analyses.

Instructions on how to run the analytics process from start to finish are in the READMEs within the any_node and master_node directories.

Note that the master_node shell scripts should be run on the master node of a docker swarm, and the any_node shell scripts can be run on any node, as they don not require swarm.
Each of the pull_ shell scripts pull images, and then the update_ shell scripts either create/update scenario databases or HTML files.

The MapReduce_code text file contains the MapReduce functions used on each of the databases used for my scenarios.