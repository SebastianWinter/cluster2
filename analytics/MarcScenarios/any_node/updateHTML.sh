docker run -e HTTPS_PROXY=http://wwwproxy.unimelb.edu.au:8000/ --volume /mnt/output/html_files:/htmloutput --name html_aqsentiment marcnguyen/ccass2:aqsentimenthtml
docker run -e HTTPS_PROXY=http://wwwproxy.unimelb.edu.au:8000/ --volume /mnt/output/html_files:/htmloutput --name html_closesthospital marcnguyen/ccass2:closesthospitalhtml
docker run -e HTTPS_PROXY=http://wwwproxy.unimelb.edu.au:8000/ --volume /mnt/output/html_files:/htmloutput --name html_isosentiment marcnguyen/ccass2:isosentimenthtml
docker run -e HTTPS_PROXY=http://wwwproxy.unimelb.edu.au:8000/ --volume /mnt/output/html_files:/htmloutput --name html_voltweetsincome marcnguyen/ccass2:voltweetsincomehtml
