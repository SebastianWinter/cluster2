"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""

As the shell scripts here run containers (rather than services), they do not require docker swarm and can be run on any node.


The pullHTMLmakers and updateHTML shell scripts will pull and run the NON COUCHDB html images, outputting the html files to a folder html_files on an attached volume to the instance which is mounted on to the docker containers.

The coviddocker.py program will run alongside the populationbystat.csv file in data_used with the command:

docker pull tpagel/comp90024:myscenario
docker run -e HTTPS_PROXY=http://wwwproxy.unimelb.edu.au:8000/ --volume /mnt/output/html_files:/htmloutput --name myscenario tpagel/comp90024:myscenario

