"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""

The SA3_2016 files are all part of a zip file from the ABS and describe the coordinate boundaries of each SA3 statistical area in Victoria.

The CreateJson.py script takes in the shape files and converts then into a json format readable by the HoloViews package used in later analyses.

The MEL_SA3_POP.json and SYD_SA3_POP comes from the SA3 Aggregated Population & Dwelling Counts 2016 Census for Australia dataset on Aurin, limited to the Greater Melbourne and Greater Sydney areas respectively. It provides the populations for each SA3 statistical area in Melbourne and Sydney. While no geospatial data was attached, it could be linked to SA3_json in order to provide a population property for each SA3 area.


The NEWSA3DATAMEL.json and NEWSA3DATASYD.json comes from the SA3-P02 Selected Medians and Averages-Census 2016  dataset on Aurin, limited to the Greater Melbourne and Greater Sydney areas respectively. It provides the median weekly incomes for each SA3 statistical area in Melbourne and Sydney. While no geospatial data was attached, it could be linked to SA3_json in order to provide an income property for each SA3 area.

The WealthJsonMaker.py and PopJsonMaker.py scripts merge the income and population features from the above AURIN json files into the SA3_JSON.json file, allowing it to include those features for analysis.

Note that the two merge python scripts can be run in either order, though the BetterJSON.json fle output by either needs to be renamed to SA3_JSON.json. Upon completion of both, the file was manually renamed to SA3_JSON.json for use in the HTML creator script.