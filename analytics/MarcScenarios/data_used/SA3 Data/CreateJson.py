"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""

import json
import shapefile
import numpy

#Reads the shape file
sf=shapefile.Reader("SA3_2016_AUST.shp")
shapes=sf.shapes() 

#Loads it into a dictionary format which can be used by HoloViews

SA3dict={}
i=0
while i<len(sf):
	try:
		identifier=sf.record(i)['SA3_MAIN16']
	    SA3dict[identifier]={}
	    SA3dict[identifier]["ID"]=sf.record(i)['SA3_5DIG16']
	    SA3dict[identifier]["Name"]=sf.record(i)['SA3_NAME16']
	    coords=[]
	    for item in shapes[i].points:
	        entry=[]
	        for coord in item:
	            entry+=[coord]
	        coords+=[entry]
	    testlats=np.array([])
	    testlongs=np.array([])
	    for coord in coords:
	        testlats=np.append(testlats,coord[1])
	        testlongs=np.append(testlongs,coord[0])
	    SA3dict[identifier]["lons"]=testlongs
	    SA3dict[identifier]["lats"]=
	except:
		continue

#Outputs the dictionary as a json file to be used by HTML analyses scripts

with open("SA3_JSON.json","w") as f:
	json.dump(SA3dict,f)