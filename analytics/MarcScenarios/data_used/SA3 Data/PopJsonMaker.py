"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""

# Loads the relevant json files
import json
with open("SA3_JSON.json") as f:
    jsondata=json.load(f)

#The below for loop is used to ensure only Melbourne and Sydney areas are kept

for item in jsondata:
    jsondata[item]["Found"]=False

with open("MEL_SA3_POP.json") as f:
    melincomedata=json.load(f)

with open("SYD_SA3_POP.json") as f:
    sydincomedata=json.load(f)

# Merges in the Melboure and Sydney population data into the main SA json

for item in melincomedata["features"]:
    ID=item["properties"]["sa3_code_2016"]
    for entry in jsondata:
        if entry==ID:
            jsondata[entry]["Population"]=item["properties"]["population"]
            jsondata[entry]["Found"]=True
            break

for item in sydincomedata["features"]:
    ID=item["properties"]["sa3_code_2016"]
    for entry in jsondata:
        if entry==ID:
            jsondata[entry]["Population"]=item["properties"]["population"]
            jsondata[entry]["Found"]=True
            break

#The following deletes any items which were not found in melbourne or sydney, as they would not be required for analyses

delarray=[]
for item in jsondata:
    if jsondata[item]["Found"]==True:
        continue
    else:
        delarray+=[item]

for item in delarray:
    del jsondata[item]

#Outputs the json.

with open("BetterJSON.json","w") as f:
    json.dump(jsondata,f)