"""

The University of Melbourne

COMP90024: Cluster and Cloud Computing

Semester 1 2020

Assignment 2: City Analytics on the Cloud

Group 69

Wei He 835655

Marc Nguyen 350899

Tom Pagel 10637290

Toby Profitt 761991

Sebastian Winter 685124

"""

The AirQuality.json comes from the UNSW CFRC - Schools Weather and Air Quality (SWAQ) Project - Air Quality Sydney (Point) 2019 dataset on AURIN. It provides readings for various metrics of air quality and coordinates for the detector sites of those readings. It required pre-processing to average readings for each site, as there were multiple readings per site.

The HOSPITALS_MEL.json and HOSPITALS_SYD.json come from the MyHospitals Profile Data - Number of Beds dataset on Aurin, limited to the Greater Melbourne and Greater Sydney areas respectively. It provides the names and coordinates of each site containing hospital beds in Melbourne and Sydney. It required filtering in order to remove all non-hospital sites.

populstioncapstate.csv comes from the ABS - Data by Region - Population & People (GCCSA) 2011-2018 dataset on AURIN. It splits australias population into states and also its capital cities and outside cap cities. This csv is from 2018 (the latest version)