#!/bin/bash
# ansible-playbook: command to run a playbook
# --ask-become-pass: need sudo permision for some of the operations: put in the password coppied from cloud
. ./openrc.sh;
ansible-playbook -i hosts --ask-become-pass nectar.yaml

#first password: from cloud website: user -> settings -> reset password
#second password: the password for the local computer running on

#can then check existance with 
# ssh -i cloud.key ubuntu@<instance ip printed during run>
# -vvv: verbose for debugging