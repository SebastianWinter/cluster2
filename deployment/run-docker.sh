#!/bin/bash

. ./openrc.sh; 
ansible-playbook -i hosts --key-file=~/.ssh/cloud.key docker.yaml

# -i hosts:
# shows where the inventory file is. This file contains nodes (ips) under different groups can then run 
# can select which group, and then run ansible against them

# -u: user:

#--key-file: path to the private key