#!/bin/bash

. ./openrc.sh; 
#host_to_be_removed: the name of the machine to be removed
ansible-playbook -i hosts spindown.yaml

# -i hosts:
# shows where the inventory file is. This file contains nodes (ips) under different groups can then run 
# can select which group, and then run ansible against them

# -u: user:

#--key-file: path to the private key