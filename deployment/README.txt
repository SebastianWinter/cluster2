REQUIRED:
The following steps must be undertaken to log to run the following playbooks

Generate a key pair:
Load the keypair for your specific project.
Your public key need to  be added to your nectar keys through https://dashboard.cloud.unimelb.edu.au/project/
Your private key should be stored at the following location: ~./.ssh/
put it in local file ~./.ssh/cloud.key (mac use cmd+shift+g and type ~./.ssh to get there)

Get Cloud password:
(from cloud website) user -> reset/generate password. copy this locally

Get Openstack RC file:
(from cloud website) User -> OpenStack RC file.
Add your openrc.sh file at path /deployment


RUNNING ANSIBLE
call the following in terminal
./run-nectar.sh

if won't run give it permission with chmod +x run-nectar.sh

You will be asked for 2 user inputs:
1 - your Cloud password, paste this in (when typing passwords no characters will appear)
2 - your local password to the account you are using on your machine. Required to execute sudo commands

LOGGING INTO AN ISTANCE
- change the ip to the machine desired
e.g
ssh -i ~/.ssh/cloud.key ubuntu@172.26.130.18


USER GUIDE:

In general there are 3 ways to specify the machines on which to run the play books. This are all set in the host_vars/general.yaml
default setting: 
- set the "instance_number" (n) and the "manager_number" (M) as described within the file. This creates M machines, the first N being manager nodes
Override setting:
- Specify the specific machine names, within the variables "override_instances", "override_managers", "override_workers" as outlined in the host_vars/general.yaml
- The playbook will only then look at these machines
Inventory file:
- The inventory file can be used to specify machine by listing the ip under the appropriate groups
- Note: to do this requires the dynamic generation for the hostfile to be stopped. Do this by setting "manual_override_host_file: True" within host_vars/general.yaml


The playbooks within deployment serve the following purposes. They can all be run using there corresponding run-*.sh file. 
They are listed in the order of precidence required to have the appropiate resources to work

Nectar: This playbook creates the required infrastcture:
- the security rules and volumes are defined in host_vars/nectar.yaml
- it requires that the available resources exist on your cloud allocation

Software: This playbook installs the standard software on each of the machines, and performs the associated task for these to be useable

Docker: This playbook enables a swarm to be formed across machines. These machine need to be specified within the host_vars/general.yaml file, as outlined above

couchDB: all the nodes to be used within a couchDB cluster need to be listed (by IP) under the '[couchDB]' group in the inventory file. 
- As this sit outside of the ansible managed block, the Inventory file setting described above isn't required.

Deploy_havester: this playbook deploys the harvester (and other) images across the swarm.
- The config of the containers to deploy is set in deployment/host_vars/deploy.yaml

front_end: This sets up the apache front end serveer. This should be deployed to a single machine.